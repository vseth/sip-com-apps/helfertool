FROM eu.gcr.io/vseth-public/nginx:echo

WORKDIR /app

# We unfortunately need pdflatex
RUN apt update && apt-get --no-install-recommends install -y          \
    texlive-base                                        \
    texlive-bibtex-extra                                \
    texlive-extra-utils                                 \
    texlive-fonts-recommended                           \
    texlive-font-utils                                  \
    texlive-latex-base                                  \
    texlive-latex-recommended                           \
    texlive-latex-extra                                 \
    texlive-pictures                                    \
    texlive-pstricks                                    \
    texlive-science                                     \
    texlive-lang-german                                 \
    perl-tk                                             \
    purifyeps                                           \
    chktex                                              \
    latexmk                                             \
    dvipng                                              \
    dvidvi                                              \
    fragmaster                                          \
    lacheck                                             \
    latexdiff                                           \
    libfile-which-perl                                  \
    dot2tex                                             \
    tipa                                                \
    libmagic1                                           \
    libsasl2-dev                                        \
    libldap2-dev                                        \
    libssl-dev                                          \
    gettext                                             \
    uwsgi                                               \
    uwsgi-plugin-python3                                \
    python3                                             \
    python3-pip                                         \
    python3-dev                                         \
    default-libmysqlclient-dev                          \
    rabbitmq-server                                     \
    wait-for-it                                         \
    libpq-dev                                           \ 
    libfreetype6-dev                                    \
    libjpeg-dev                                         \ 
    zlib1g-dev                                          \
    build-essential

COPY --chown=app-user:app-user helfertool helfertool
RUN pip3 install -U pip
RUN pip3 install mysqlclient wheel
RUN pip3 install -r helfertool/src/requirements.txt

COPY --chown=app-user:app-user build-inject/helpers.html helfertool/registration/templates/registration/privacy/helpers.html
COPY --chown=app-user:app-user build-inject/excel.py helfertool/registration/export/excel.py

WORKDIR /app/helfertool/src
ENV HELFERTOOL_CONFIG_FILE /dev/null
RUN python3 manage.py collectstatic --noinput && chmod -R o+rx /app/helfertool/static
RUN python3 manage.py makemessages -i manage.py -l de
ENV HELFERTOOL_CONFIG_FILE /app/helfertool/src/helfertool.yaml

COPY --chown=app-user:app-user config/helfertool.yml helfertool.yaml
COPY config/uwsgi.ini /etc/uwsgi/apps-enabled/helfertool.ini
COPY config/nginx.conf /etc/nginx/sites-enabled/default
COPY config/cinit.yml /etc/cinit.d/helfertool.yml
