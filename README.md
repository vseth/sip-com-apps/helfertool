# SIP Helfertool

SIP Integration of [TUM Helfertools](https://github.com/helfertool/helfertool).

## Dev setup

Build and start the container with

```
docker-compose up --build
```

## ToDo

Remove the need for excel.py build injection and create PR and add to main Repo

Remove VSETH branding for arbitrary organisations

Replace submodules
